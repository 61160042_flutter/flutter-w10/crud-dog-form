import 'package:flutter/material.dart';

import 'dog_widget.dart';

void main() {
  runApp(MaterialApp(
    title: 'Dog App',
    home: DogApp(),
  ));
}
